<?php

namespace Drupal\version_formatter\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\Plugin\Field\FieldFormatter\NumericFormatterBase;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Form\FormStateInterface;

/**
 * Plugin implementation of the 'version' formatter.
 *
 * @FieldFormatter(
 *   id = "version_formatter",
 *   label = @Translation("Version"),
 *   field_types = {
 *     "integer"
 *   }
 * )
 */
class VersionFormatter extends NumericFormatterBase {

 /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return [
      'prefix_suffix' => TRUE,
    ];
  }

/**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $elements['prefix_suffix'] = [
      '#type' => 'checkbox',
      '#title' => t('Display prefix and suffix'),
      '#default_value' => $this->getSetting('prefix_suffix'),
      '#weight' => 10,
    ];

    return $elements;
  }


  /**
   * {@inheritdoc}
   */
  protected function numberFormat($number) {
      $value = (string) $number; 
      $value = preg_split('//', $value, -1, PREG_SPLIT_NO_EMPTY);
      $value = implode('.',$value);
      return $value;
  }
}
