Version Formatter


Small module helps to display the numeric integer field as numeric versions by enabling the version field Formatter,

For example

You need to create a content type which needs to have simple versions to integrate with third party service.

You can just create a simple integer field. you get version numbers from users (content authors/admin when they add content using node form) or you can increment version when the content is been saved by using hook_entity_presave and in the formatter if you enable the version the numbers will be displayed as versions

Example 100 can be displayed as 1.0.0 or v1.0.0 or v1.0.0v or 1.0.1v here v is configurable, Any character or symbols can be configured by using prefix suffix settings at default field settings.

Instruction to use the module

1. Install the module
 
2. Create a field  numeric integer field for a content type that you want to attach version

3. Go to manage display and select version as formatter for the created numeric integer

4. now create a content with the value 100 as version in created numeric field then see the node view page


You can also automate the above 4th step by automatically increment when you saving the node.

Note : Disable the version field from "Manage form display" to use autoincrement code so that user don't have
access to add version manually

<?php 

use \Drupal\Core\Database\Database;

/**
 * Implements hook_entitypresave().
 */
  function module_node_presave($entity) {
    if($entity->bundle() == "yourcontenttype" && !$entity->isNew()) {
    // To support increment of version with latest version number when revert to older revisions 
    $conn = Database::getConnection();
    $query = $conn->select('node_revision__field_name', 'fc');
    $query->condition('fc.entity_id', $entity->id(), '=');
    $query->fields('fc', ['field_name_value']);
    $query->orderBy('fc.field_name_value', 'DESC');
    $query->range(0, 1);
    $result = $query->execute(); 
    $entity_version = 100;
    foreach ($result as $value) {
      $entity_version = $value->field_name_value;
    }       
   /**
    * You can also add your own logical condition to increment a version. (eg)
    * Instead of increment the version on content update, increment a version when the * 
    * content changed to particular moderation state  from another state.
   */       
     $entity->set('field_name', $entity_version + 1); 
    }
  }



